import React, { useState, useEffect, useRef } from 'react';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import 'animate.css';
import SelectColor from './components/SelectColor';
import Grid from './components/Grid';
import html2canvas from 'html2canvas';
import Swal from 'sweetalert2'
import logo from './images/empty.png'
import puppy from './images/puppy.png'
import error from './images/error.png'

function App() {
  const gridInit = Array(100).fill('#FFF');

  //USE STATE

  //Arreglo que almacena 10 colores. Primero verifica si hay colores 
  //en el local storage, si no hay empty array
  let [colorOptions, setColorOptions] = useState(
    window.localStorage.getItem('colorOptions') ? window.localStorage.getItem('colorOptions').split(',') : [])

  //Color indica el color seleccionado, su valor inicial es el color de la posición 0 del 
  //array colorOptions
  let [color, setColor] = useState(
    colorOptions[0]
  );

  //Indica el estado de la peticion a la API de Hexbot: loading, resolved y rejected
  let [colorOptionsState, setColorOptionsState] = useState('loading');

  //Grid array, arreglo de 100 elementos donde se almacena el color de cada uno de ellos
  //Si hay valores almacenado en el local storage, los toma; de otra forma le asigna el valor de gridInit  
  let [gridArray, setGridArray] = useState(
    window.localStorage.getItem('gridArray') ? window.localStorage.getItem('gridArray').split(',') : gridInit)

  //Estado de la impresión: true o false
  let [printing, setPrinting] = useState(false);

  //Almacena en el local storage los colores que tiene cada box del grid
  //Almacena en el local storage la paleta de colores
  useEffect(() => {
    window.localStorage.setItem('colorOptions', colorOptions);
    window.localStorage.setItem('gridArray', gridArray);
  });

  //Lo que pasa cada vez que se recarga la página
  //Este solo es cada vez que se recarga la página, el otro se realiza todo el tiempo 
  useEffect(() => {

    //Si es la primera vez que se carga la página, manda llamar a la función changeColors()
    if (window.localStorage.getItem('colorOptions') === '')
      changeColors();

    //Si ya hay colores almacenados en la local storage, hace un set con los colores almacenados
    else {
      let newColors = window.localStorage.getItem('colorOptions').split(',')
      setColorOptions(newColors);
      setColor(newColors[0]);
      setColorOptionsState('resolved')
    }

    //Cuando se recarga la página se obtiene un número aleatorio entre 0 y 1
    //Si este es mayor o igual a .7, el usuario tendrá la posibilidad de personalizar la paleta de colores.
    if (Math.random() >= 0.5) {
      random.current.removeAttribute('hidden');
      Swal.fire({
        showConfirmButton: false,
        icon: 'info',
        title: `You're lucky!`,
        timer: 3000,
        html: 'You can chose you favorites colors, try it!',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      })
    }
  }, [])

  //UseRef -> Referencias a cada elemento
  const draw = useRef(); //hace referencia al grid 
  const snapshot = useRef(); //hace referencia al anchor con la imagen que se imprime
  const random = useRef(); //hace referencia al input color
  const emptyLogo = useRef(); //hace referencia a la imagen que se muestra cuando no se ha impreso

  //Funcion para crear un nuevo juego
  function newGame() {
    setGridArray(gridInit); //se limpia el tablero
    snapshot.current.innerHTML = ' '; //Elimina los elementos dentro de snapshot
    snapshot.current.setAttribute('disabled', true); //agrega el atributo disabled
    snapshot.current.setAttribute('href', ''); //href vacio
    emptyLogo.current.removeAttribute('hidden'); //hace visible la imagen por default
  }

  //Función para cambiar la paleta de colores
  function changeColors() {
    const newColors = []
    setColorOptionsState('loading');

    //Peticion http a hexbot, regresa 10 colores
    fetch('https://api.noopschallenge.com/hexbot?count=10').then(response => //Response object
      response.json() //response obj -> json
    ).then(myJson => //json tiene array de obj colors con atributo value -> color en hx
      myJson.colors.map(item => newColors.push(item.value)) //se mapea cada obj json y se agrega a newColors
    ).then(() => {
      setColorOptions(newColors);
      setColor(newColors[0]);
      setColorOptionsState('resolved')
    }).catch(() => {
      Swal.fire({
        icon: 'error',
        title: 'Woof!!!',
        text: "We can't obtain more beautiful colors",
        imageUrl: error,
        imageWidth: 200,
        imageAlt: 'Custom image',
      })
      setColorOptionsState('rejected');//Estado rechazado

      if (window.localStorage.getItem('colorOptions')) {
        setColorOptions(window.localStorage.getItem('colorOptions').split(','))
        setColorOptionsState('resolved')
        random.current.removeAttribute('hidden');
      } else
        random.current.setAttribute('hidden', 'true');
    })
  }

  //Funcion asigna a la opcion seleccionada el color deseado
  function selectMyColor() {
    const myColor = random.current.value;
    setColor(myColor); //es el color que ahora tendrá el pincel
    const newColorOptions = [...colorOptions];
    newColorOptions[colorOptions.indexOf(color)] = myColor; //se modifica el color del arreglo
    setColorOptions(newColorOptions);
  }

  //Cambiar el color de una box del grid
  function onSectionClick(index) {
    const newGridArray = [...gridArray]; //trae los valores del array acctual
    newGridArray[index] = color; //se modifica el array en la posicion que se tocó
    setGridArray(newGridArray); //actualiza el array
  }

  //Verifica que la box sea color blanco
  const isBlank = color => color === "#FFF";

  function printDraw() {
    if (!gridArray.every(isBlank)) { //Si se ha coloreado el grid -> si no está en blanco
      setPrinting(true); //Pone el estado de printing en true
      emptyLogo.current.setAttribute('hidden', true); //se oculta la imagen por default
      let c = draw.current.children  //c = array de grid/draw
      for (let i = 0; i < c.length; i++) { //a cada uno de los box se les agregan las siguientes clases
        c[i].classList.add('bordercero')
        c[i].classList.add('boxWithoutBorders')
      }
      window.scrollTo(0, 0); // <-- this fixes the issue
      html2canvas(draw.current).then(canvas => {
        snapshot.current.innerHTML = ' ';
        snapshot.current.appendChild(canvas);
        snapshot.current.children[0].classList.add('canvas')
        setPrinting(false);
        snapshot.current.setAttribute('href', canvas.toDataURL());
        snapshot.current.setAttribute('download', Date.now().toString()); //nombbre del descargable
        snapshot.current.removeAttribute('disabled');
      });
      for (let i = 0; i < c.length; i++) { //se le vuelven a agregar las clases
        c[i].classList.remove('bordercero')
        c[i].classList.remove('boxWithoutBorders')
      }
      Swal.fire({
        showConfirmButton: false,
        icon: 'info',
        timer: 2000,
        title: 'Click your drawing to download...',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      })
    } else
      Swal.fire({
        title: 'Woof!!!',
        text: 'Try draw something!',
        imageUrl: error,
        imageWidth: 200,
        imageAlt: 'Custom image',
      })
  }

  function downloading() {
    Swal.fire({
      showConfirmButton: false,
      icon: 'success',
      timer: 2000,
      title: 'Downloading...',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
  }
  return (
    <div className="container">
      <div className="row"><div className="col-md-12 col-12 centerItems">
        <img id="puppy" src={puppy} alt='puppy' />
      </div></div>
      <div className="row marginTop">
        <div id="buttons" className="col-md-6 col-12 ">
          {/* Botón new game */}
          <button
            className="button"
            type="button"
            onClick={newGame}
          >New game</button>

          {/*Botón para cambiar la paleta de colores*/}
          <button
            className="button"
            type="button"
            onClick={changeColors}
          >Change colors</button>

          {/* Botón para mandar a imprimir el dibujo */}
          <button
            className="button"
            type="button"
            onClick={printDraw}
          >Print grid</button>

          {/*Input para elegir el color deseado*/}
          <input
            type='color'
            id="random"
            ref={random}
            className="colorPicker"
            hidden
            onChange={selectMyColor}
          />
        </div>

        {/*Componente con la paleta de colores*/}
        <SelectColor
          colors={colorOptions}
          selectedColor={color}
          ifSelected={setColor}
          colorOptionsState={colorOptionsState}
        />
      </div>
      <div className="row marginTop marginBottom">
        <div className="col-md-6 col-12 marginTop">
          <Grid
            draw={draw}
            grid={gridArray}
            onSectionClick={onSectionClick}
            printing={printing}
          />
        </div>
        <div id="imageSection" className="col-md-6 col-12 marginTop border" >
          <img ref={emptyLogo} alt='logo' src={logo} width="100%" />
          <a id="snapshot" ref={snapshot} href="http:\\perritos.com" onClick={downloading} disabled />
        </div>

      </div>
    </div>
  );
}

export default App;
