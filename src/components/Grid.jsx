import React from 'react';
import PropTypes from 'prop-types';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';

function Grid(props) {

    //Destructuring 
    const {
        draw, //referencia
        grid, //gridArray
        onSectionClick,
        printing
    } = props;
    //se ejecuta cada vez que el mouse pasa por encima de un cuadrito
    function onDrawing(index, moseEvent) {
        if (moseEvent.buttons === 1) {
            onSectionClick(index); //cambia el color de la box en la posicion index
        }
    }
    function changeColor(index) {
        onSectionClick(index);
    }
    return (
        <div id='draw' ref={draw} className="grid" >
            {grid.map((box, index) =>{  //por cada elemento del grid array genera una box
               return (<div
                    className='box'
                    //si pinting es false el color de será blanco
                    style={{ backgroundColor: printing ? (box === '#FFF' ? '#CFD8DC' : box) : box }}     //aqui colorea los cuadritos del color selecionado
                    key={index}
                    onClick={() => changeColor(index)} //cuando se hace click se manda llamar changecolor
                    onMouseMove={(moseEvent) => onDrawing(index, moseEvent)}
                />)
            })}
        </div>
    )
}

Grid.propTypes = {
    grid: PropTypes.array.isRequired,
    onSectionClick: PropTypes.func.isRequired,

}
export default Grid;
