import React from 'react';
import PropTypes from 'prop-types';
import '../App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPaw } from '@fortawesome/free-solid-svg-icons'
import loading from '../images/loading.gif'

function SelectColor(props) {
    //Destructuring
    const {
        colors,
        selectedColor,
        ifSelected,
        colorOptionsState
    } = props;

    //Switch del estado de color options
    switch (colorOptionsState) {
        case 'rejected': //si el estado es rechazado se muestra un mensaje
            return (
                <div id="colors" className="col-md-6 col-12">
                    Sorry we can't paint without colors
                </div>
            )
        case 'loading': //si se está cargando se muestra el gif
            return (
                <div className="loading col-md-6 col-12">
                    <img alt='Loading...' src={loading} style={{ height: '2em' }} />
                </div>
            )
        default:
            return (
                <div id="colors" className="col-md-6 col-12">
                    {colors.map((color, index) => {
                        const isSelected = color === selectedColor; //si el color del icono es igual al color del array
                        return (
                            <div
                                style={{ color: color }}
                                key={index}
                                className={`colorOption  ${isSelected ? 'colorOptionSelected selectcolor' : 'selectcolor'}`}
                                onClick={() => ifSelected(color)} >
                                <FontAwesomeIcon icon={faPaw} />
                            </div>
                        )
                    })}
                </div>
            )
    }

}
SelectColor.propTypes = {
    colors: PropTypes.array,
    selectedColor: PropTypes.string,
    ifSelected: PropTypes.func.isRequired,
    colorOptionsState: PropTypes.string.isRequired

}
export default SelectColor;
